# Step 1: Build Rust
FROM rust:latest as rust-builder

WORKDIR /usr/src/myapp
COPY migrate_csplus/ ./migrate_csplus

RUN cargo build --release --manifest-path migrate_csplus/Cargo.toml

# Step 2: Deno image
FROM denoland/deno:alpine

WORKDIR /app

COPY --from=rust-builder /usr/src/myapp/migrate_csplus/target/release/migrate_csplus ./migrate_csplus/target/release/migrate_csplus

COPY index.ts .
COPY lib ./lib
ENV PRODUCTION=true

# cache deps
RUN deno cache index.ts

EXPOSE 3000

CMD ["deno", "run", "--allow-env", "--allow-read", "--allow-write", "--allow-net", "--allow-run", "index.ts"]
