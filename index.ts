import {
  Application,
  Context,
  Router,
} from "https://deno.land/x/oak@v12.6.1/mod.ts";
import { Client } from "https://deno.land/x/postgres@v0.17.0/mod.ts";
import { load } from "https://deno.land/std@0.209.0/dotenv/mod.ts";
import { updateProfile } from "./lib/update_profile.ts";
import { deleteProfile } from "./lib/delete_profile.ts";
import { migrateProfile } from "./lib/migrate_profile.ts";
import {
  isProfilesTableEmpty,
  runCsplusImport,
  waitForTableCreation,
} from "./lib/utils.ts";
import ApiDuniter from "./lib/duniter_connect.ts";
import { DuniterService } from "./lib/duniter_service.ts";
import { addTransaction } from "./lib/add_transaction.ts";

// Determine the environment
const isProduction = Deno.env.get("PRODUCTION") === "true";

// Load environment variables based on the environment
const env = isProduction ? Deno.env.toObject() : await load();

// Assign values from environment variables
const dbUser = env["DB_USER"];
const dbDatabase = env["DB_DATABASE"];
const dbPassword = env["DB_PASSWORD"];
const dbHostname = isProduction ? "postgres-datapod" : "localhost";
const importCsplusData = env["IMPORT_CSPLUS_DATA"] === "true";
const duniterEndpoints = env["DUNITER_ENDPOINTS"];
const duniterEndpointsArray = JSON.parse(duniterEndpoints.replace(/'/g, '"'));
const hasuraPort = env["HASURA_LISTEN_PORT"];
const dbPort = 5432;

const client = new Client({
  user: dbUser,
  database: dbDatabase,
  hostname: dbHostname,
  password: dbPassword,
  port: dbPort,
});

await client.connect().catch((error) => {
  console.error(error);
  Deno.exit(1);
});

const app = new Application();
const router = new Router();

// Connect to Duniter
await ApiDuniter.getInstance(duniterEndpointsArray);

// Wait for table creation before continue
await waitForTableCreation(client, "public.profiles").catch((error) => {
  console.error(error);
  Deno.exit(1);
});

// Subscribe to blockchain events
const duniterService = new DuniterService();
await duniterService.subscribeEvents(client).catch(console.error);

// Import Cs+ data
const profilesEmpty = await isProfilesTableEmpty(client);
if (profilesEmpty && importCsplusData) {
  await runCsplusImport(isProduction).catch((error) => {
    console.error(error);
    Deno.exit(1);
  });
}

// Manage routes
router.post(
  "/update-profile-data",
  async (ctx: Context) => await updateProfile(ctx, client),
);
router.post(
  "/delete-profile-data",
  async (ctx: Context) => await deleteProfile(ctx, client),
);
router.post(
  "/migrate-profile-data",
  async (ctx: Context) => await migrateProfile(ctx, client),
);
router.post(
  "/add-transaction",
  async (ctx: Context) => await addTransaction(ctx, client),
);

app.use(router.routes());
app.use(router.allowedMethods());

console.log(`Datapod is started: http://localhost:${hasuraPort}\n`);
await app.listen({ port: 3000 });
