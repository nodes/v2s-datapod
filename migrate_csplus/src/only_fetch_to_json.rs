use reqwest;
use serde::{Deserialize, Serialize};
use serde_json::Value;
use std::collections::HashMap;
use std::error::Error;
use std::fs::File;
use std::io::Write;
use std::env;

#[derive(Serialize, Deserialize, Debug)]
struct Hit {
    #[serde(rename = "_source")]
    source: HashMap<String, Value>,
}

#[derive(Serialize, Deserialize, Debug)]
struct ScrollResponse {
    #[serde(rename = "_scroll_id")]
    scroll_id: String,
    hits: Hits,
}

#[derive(Serialize, Deserialize, Debug)]
struct Hits {
    hits: Vec<Hit>,
}

async fn fetch_scroll_batch(scroll_id: &str, scroll_time: &str) -> Result<ScrollResponse, Box<dyn Error>> {
    let client = reqwest::Client::new();
    let response = client
        .post("https://g1.data.e-is.pro/_search/scroll")
        .json(&serde_json::json!({ "scroll": scroll_time, "scroll_id": scroll_id }))
        .send()
        .await?
        .json::<ScrollResponse>()
        .await?;
    Ok(response)
}

fn process_hits(hits: &[Hit]) -> Vec<HashMap<String, Value>> {
    hits.iter().map(|hit| {
        let mut profile = hit.source.clone();
        profile.remove("signature");
        profile.remove("version");
        profile.remove("hash");
        profile.remove("tags");
        profile
    }).collect()
}

async fn fetch_profiles() -> Result<(), Box<dyn Error>> {
    let args: Vec<String> = env::args().collect();

    let get_avatar = if args.len() > 1 {
        args[1].parse::<bool>().unwrap_or(false)
    } else {
        false
    };

    let mut url = String::from("https://g1.data.e-is.pro/user/profile/_search?scroll=5m");
    
    if !get_avatar {
        println!("Avatars are skipped");
        url.push_str("&_source_exclude=avatar._content");
    } else {
        println!("Avatars are recovered");
     }
    

    let scroll_time = "5m"; // Scroll duration
    let page_size = 5000; // Page size
    let initial_response = reqwest::Client::new()
        .post(url)
        .json(&serde_json::json!({ "query": { "match_all": {} }, "size": page_size }))
        .send()
        .await?
        .json::<ScrollResponse>()
        .await?;

    let mut scroll_id = initial_response.scroll_id;
    let mut profiles: Vec<HashMap<String, Value>> = process_hits(&initial_response.hits.hits);
    let mut iteration = 0;

    while !scroll_id.is_empty() {
        iteration += 1;
        println!("Iteration: {}", iteration);

        let response = fetch_scroll_batch(&scroll_id, scroll_time).await?;
        let new_profiles = process_hits(&response.hits.hits);

        if new_profiles.is_empty() {
            break;
        }

        profiles.extend(new_profiles);
        scroll_id = response.scroll_id;
    }

    let mut file = File::create("profile_csplus.json")?;
    write!(file, "{}", serde_json::to_string(&profiles)?)?;

    Ok(())
}

#[tokio::main]
async fn main() {
    if let Err(e) = fetch_profiles().await {
        eprintln!("Error: {}", e);
    }
}