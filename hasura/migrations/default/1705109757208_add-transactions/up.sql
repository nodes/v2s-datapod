SET check_function_bodies = false;
CREATE TABLE public.profiles (
    address text NOT NULL,
    avatar bytea,
    description text,
    geoloc point,
    title text,
    city text,
    socials jsonb,
    created_at timestamp with time zone DEFAULT CURRENT_TIMESTAMP,
    updated_at timestamp with time zone DEFAULT CURRENT_TIMESTAMP
);
CREATE FUNCTION public.bytea_to_base64(data_row public.profiles) RETURNS text
    LANGUAGE plpgsql STABLE
    AS $$
BEGIN
    RETURN ENCODE(data_row.avatar, 'base64');
END;
$$;
CREATE TABLE public.transactions (
    id text NOT NULL,
    comment text NOT NULL,
    created_at timestamp with time zone DEFAULT CURRENT_TIMESTAMP
);
COMMENT ON TABLE public.transactions IS 'Store transactions comments';
ALTER TABLE ONLY public.profiles
    ADD CONSTRAINT profiles_pkey PRIMARY KEY (address);
ALTER TABLE ONLY public.transactions
    ADD CONSTRAINT transactions_pkey PRIMARY KEY (id);
