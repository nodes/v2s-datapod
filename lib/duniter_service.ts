import ApiDuniter from "./duniter_connect.ts";
import { ApiPromise } from "https://deno.land/x/polkadot@0.2.45/api/mod.ts";
import { type EventRecord } from "https://deno.land/x/polkadot@0.2.45/types/interfaces/system/index.ts";
import { BN } from "https://deno.land/x/polkadot@0.2.45/util/bn/index.ts";
import { Client } from "https://deno.land/x/postgres@v0.17.0/client.ts";
import { checkRecordExist } from "./utils.ts";

export class DuniterService {
  private api!: ApiPromise;

  constructor() {}

  private async init(): Promise<void> {
    this.api = await ApiDuniter.getInstance();
  }

  public async getBalance(address: string): Promise<number> {
    await this.init();
    const account = await this.api.query.system.account(address);
    const balanceFree = account.data.free.toNumber();

    if (balanceFree === null) {
      return 0;
    }

    const idtyIndex = await this.getIdentityIndex(address);
    if (idtyIndex === null) {
      return balanceFree;
    }

    const idtyData = await this.getIdentityData(idtyIndex);
    const unclaimedUds = idtyData ? await this.computeUnclaimUds(idtyData) : 0;

    return balanceFree + unclaimedUds;
  }

  private async getIdentityIndex(address: string): Promise<number | null> {
    const idtyIndexOption = await this.api.query.identity.identityIndexOf(
      address,
    );
    return idtyIndexOption.isSome ? idtyIndexOption.unwrap().toNumber() : null;
  }

  private async getIdentityData(
    idtyIndex: number,
  ): Promise<Record<string, Record<string, BN>> | null> {
    const idtyDataOption = await this.api.query.identity.identities(idtyIndex);
    return idtyDataOption.isSome ? idtyDataOption.unwrap() : null;
  }

  private async computeUnclaimUds(
    idtyData: Record<string, Record<string, BN>>,
  ): Promise<number> {
    const pastReevals: BN[][] = await this.api.query.universalDividend
      .pastReevals();
    const firstEligibleUd = idtyData["data"]["firstEligibleUd"].toNumber();

    if (firstEligibleUd === 0) {
      return 0;
    }

    let totalAmount = 0;
    let currentUdIndex =
      (await this.api.query.universalDividend.currentUdIndex()).toNumber();

    for (const reval of pastReevals.reverse()) {
      const [revalNbr, revalValue] = reval.map((r) => r.toNumber());

      if (revalNbr <= firstEligibleUd) {
        totalAmount += (currentUdIndex - firstEligibleUd) * revalValue;
        break;
      } else {
        totalAmount += (currentUdIndex - revalNbr) * revalValue;
        currentUdIndex = revalNbr;
      }
    }
    return totalAmount;
  }

  /// Subscribe to all Duniter blockchain events, and apply actions based on these events.
  public async subscribeEvents(client: Client) {
    const api = await ApiDuniter.getInstance();

    // Listen events
    await api.query.system.events((result: EventRecord[]) => {
      if (!result) {
        console.error("Result is undefined");
        return;
      }

      // Loop on each events received
      result.forEach(async (event) => {
        const section = event.event.section;
        const method = event.event.method;

        // If event is a killed account event, we delete the profile
        if (section === "system" && method === "KilledAccount") {
          const killedAddress = event.event.data[0].toString();

          // Verify if profile exists
          if (!await checkRecordExist(client, 'profiles', 'address', killedAddress)) {
            console.log(`Profile ${killedAddress} does not exist.`);
            return;
          }

          // Prepare and execute database query for deletion
          const deleteQuery = `
          DELETE FROM profiles
          WHERE address = $1;
          `;

          try {
            await client.queryObject({
              text: deleteQuery,
              args: [killedAddress],
            });
            console.log(`Profile ${killedAddress} has been deleted`);
          } catch (error) {
            console.error(
              "Database error in deleting ${killedAddress} profile:",
              error,
            );
          }
        }
      });
    });
  }
}
