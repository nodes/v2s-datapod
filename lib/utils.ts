import { Client } from "https://deno.land/x/postgres@v0.17.0/client.ts";

export function convertBase64ToBytea(
  base64String: string | null | undefined,
): Uint8Array | null {
  if (base64String === null || base64String === undefined) {
    return null;
  }

  // Remove the MIME type prefix from the base64 string, if present
  const base64Data = base64String.split(",")[1] || base64String;

  // Convert the base64 string to a binary string
  const binaryString = atob(base64Data);

  // Convert the binary string to a Uint8Array
  const bytes = new Uint8Array(binaryString.length);
  for (let i = 0; i < binaryString.length; i++) {
    bytes[i] = binaryString.charCodeAt(i);
  }

  return bytes;
}

export async function runCsplusImport(isProduction: boolean) {
  const command = new Deno.Command(
    "./migrate_csplus/target/release/migrate_csplus",
    {
      env: { "PRODUCTION": isProduction.toString() },
      stdout: "piped",
      stderr: "piped",
    },
  );

  const process = command.spawn();

  process.stdout.pipeTo(Deno.stdout.writable, {
    preventClose: true,
    preventCancel: true,
    preventAbort: true,
  });
  process.stderr.pipeTo(Deno.stderr.writable, {
    preventClose: true,
    preventCancel: true,
    preventAbort: true,
  });

  const status = await process.status;

  if (!status.success) {
    throw new Error(`Process migrate_csplus exited with code: ${status.code}`);
  }
}

export async function isProfilesTableEmpty(client: Client): Promise<boolean> {
  const result = await client.queryObject<{ count: bigint }>(
    "SELECT COUNT(*) FROM public.profiles",
  );
  return result.rows[0].count === 0n;
}

interface TableCheckResult {
  to_regclass: string | null;
}

async function checkTableExists(
  client: Client,
  tableName: string,
): Promise<boolean> {
  try {
    const result = await client.queryObject<TableCheckResult>(
      `SELECT to_regclass('${tableName}')`,
    );
    return result.rows[0].to_regclass !== null;
  } catch (error) {
    console.error("Error checking table existence:", error);
    return false;
  }
}
function delay(ms: number): Promise<void> {
  return new Promise((resolve) => setTimeout(resolve, ms));
}

export async function waitForTableCreation(
  client: Client,
  tableName: string,
  maxAttempts = 10,
) {
  let attempts = 0;
  while (attempts < maxAttempts) {
    if (await checkTableExists(client, tableName)) {
      return;
    }

    await delay(1000);
    attempts++;
  }

  throw new Error(`Table ${tableName} not found after ${maxAttempts} try.`);
}

export async function checkRecordExist(
  client: Client,
  tableName: string,
  columnName: string,
  value: string
): Promise<boolean> {
  const query = `SELECT 1 FROM ${tableName} WHERE ${columnName} = $1;`;
  const result = await client.queryObject({
    text: query,
    args: [value],
  });
  const rowCount = result.rowCount ?? 0;
  return rowCount > 0;
}

