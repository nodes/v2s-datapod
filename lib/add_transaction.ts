import { Context } from "https://deno.land/x/oak@v12.6.1/context.ts";
import { Client } from "https://deno.land/x/postgres@v0.17.0/client.ts";
import {
  SignatureResponse,
  signatureResponseMessages,
  verifySignature,
} from "./signature_verify.ts";
import { Transaction } from "./types.ts";
import { checkRecordExist } from "./utils.ts";

export async function addTransaction(ctx: Context, client: Client) {
  try {
    const body = await ctx.request.body().value;
    const transaction: Transaction = body.variables || body.input || {};

    // Validate input
    if (!transaction.id || !transaction.comment) {
      ctx.response.status = 400;
      ctx.response.body = {
        success: false,
        message: "transaction id and comment are required.",
      };
      return;
    }

    // Verify signature
    const signatureResult = await verifySignature(transaction);
    if (signatureResult !== SignatureResponse.valid) {
      ctx.response.status = 400;
      console.error(
        "Invalid signature: " + signatureResponseMessages[signatureResult],
      );
      ctx.response.body = {
        success: false,
        message: "Invalid signature: " +
          signatureResponseMessages[signatureResult],
      };
      return;
    }

    // Verify new profile doesn't exists
    if (await checkRecordExist(client, 'transactions', 'id', transaction.id)) {
      ctx.response.status = 422;
      console.error(`ID ${transaction.id} already exist.`);
      ctx.response.body = {
        success: false,
        message: `ID ${transaction.id} already exist.`,
      };
      return;
    }

    // Prepare and execute database query for deletion
    const addTransactionQuery = `
    INSERT INTO transactions (id, comment)
    VALUES ($1, $2);
    `;


    try {
      await client.queryObject({
        text: addTransactionQuery,
        args: [transaction.id, transaction.comment],
      });
      ctx.response.status = 200;
      console.log(
        `Transaction with ID ${transaction.id} has been insert.`,
      );
      ctx.response.body = {
        success: true,
        message:
          `Transaction with ID ${transaction.id} has been insert.`,
      };
    } catch (error) {
      console.error("Database error in insert transaction:", error);
      ctx.response.status = 500;
      ctx.response.body = {
        success: false,
        message: "Internal server error: " + error,
      };
    }
  } catch (error) {
    console.error("Error insert transaction:", error);
    ctx.response.status = 500;
    ctx.response.body = {
      success: false,
      message: "Error insert transaction: " + error,
    };
  }
}
