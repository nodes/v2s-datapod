type Geolocation = { latitude: number | null; longitude: number | null };
type SocialInfo = { url: string; type: string };

export type Profile = {
  address: string;
  hash: string;
  signature: string;
  avatarBase64?: string | null;
  description?: string;
  geoloc?: Geolocation;
  title?: string;
  city?: string;
  socials?: SocialInfo[];
  oldAddress?: string;
};

export type Transaction = {
  id: string;
  address: string;
  comment: string;
  hash: string;
  signature: string;
}
