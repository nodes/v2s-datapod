import { Context } from "https://deno.land/x/oak@v12.6.1/context.ts";
import { Client } from "https://deno.land/x/postgres@v0.17.0/client.ts";
import { Profile } from "./types.ts";
import { SignatureResponse, verifySignature } from "./signature_verify.ts";
import { checkRecordExist } from "./utils.ts";

export async function deleteProfile(ctx: Context, client: Client) {
  try {
    const body = await ctx.request.body().value;
    const profile: Profile = body.variables || body.input || {};

    // Verify signature
    const signatureResult = await verifySignature(profile);
    if (signatureResult !== SignatureResponse.valid) {
      ctx.response.status = 400;
      console.error("Invalid signature: " + SignatureResponse[signatureResult]);
      ctx.response.body = {
        success: false,
        message: "Invalid signature: " + SignatureResponse[signatureResult],
      };
      return;
    }

    // Verify if profile exists
    if (!await checkRecordExist(client, 'profiles', 'address', profile.address)) {
      ctx.response.status = 404;
      console.error(`Profile ${profile.address} does not exist.`);
      ctx.response.body = {
        success: false,
        message: `Profile ${profile.address} does not exist.`,
      };
      return;
    }

    // Prepare and execute database query for deletion
    const deleteQuery = `
    DELETE FROM profiles
    WHERE address = $1;
    `;

    try {
      await client.queryObject({
        text: deleteQuery,
        args: [profile.address],
      });
      ctx.response.status = 200;
      console.log(`Profile ${profile.address} has been deleted`);
      ctx.response.body = {
        success: true,
        message: `Profile ${profile.address} has been deleted`,
      };
    } catch (error) {
      console.error(
        `Database error in deleting ${profile.address} profile:`,
        error,
      );
      ctx.response.status = 500;
      ctx.response.body = {
        success: false,
        message: "Internal server error: " + error,
      };
    }
  } catch (error) {
    console.error("Error deleting profile:", error);
    ctx.response.status = 500;
    ctx.response.body = {
      success: false,
      message: "Error deleting profile: " + error,
    };
  }
}
