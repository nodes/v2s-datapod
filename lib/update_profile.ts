import { Context } from "https://deno.land/x/oak@v12.6.1/context.ts";
import { Client } from "https://deno.land/x/postgres@v0.17.0/client.ts";
import { SignatureResponse, verifySignature } from "./signature_verify.ts";
import { checkRecordExist, convertBase64ToBytea } from "./utils.ts";
import { Profile } from "./types.ts";
import { DuniterService } from "./duniter_service.ts";

export async function updateProfile(ctx: Context, client: Client) {
  try {
    const body = await ctx.request.body().value;
    const profile: Profile = body.variables || body.input || {};
    const socialJson = JSON.stringify(profile.socials);

    // Verify signature
    const signatureResult = await verifySignature(profile);
    if (signatureResult !== SignatureResponse.valid) {
      ctx.response.status = 400;
      console.error("Invalid signature: " + SignatureResponse[signatureResult]);
      ctx.response.body = {
        success: false,
        message: "Invalid signature: " + SignatureResponse[signatureResult],
      };
      return;
    }

    // Verify wallet exist in blockchain
    const balanceService = new DuniterService();
    if (
      !await checkRecordExist(client, 'profiles', 'address', profile.address) &&
      await balanceService.getBalance(profile.address) === 0
    ) {
      ctx.response.status = 400;
      console.error(`Wallet ${profile.address} doesn't exist in blockchain.`);
      ctx.response.body = {
        success: false,
        message: `Wallet ${profile.address} doesn't exist in blockchain.`,
      };
      return;
    }

    // Convert avatar to bytes
    const avatarBytea = convertBase64ToBytea(
      profile.avatarBase64,
    );

    // Prepare and execute database query
    const query = `
    INSERT INTO profiles (address, description, avatar, geoloc, title, city, socials, created_at, updated_at)
    VALUES ($1, $2, $3, point($4, $5), $6, $7, $8, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP)
    ON CONFLICT (address) 
    DO UPDATE SET
        description = COALESCE(EXCLUDED.description, profiles.description),
        avatar = COALESCE(EXCLUDED.avatar, profiles.avatar),
        geoloc = COALESCE(EXCLUDED.geoloc, profiles.geoloc),
        title = COALESCE(EXCLUDED.title, profiles.title),
        city = COALESCE(EXCLUDED.city, profiles.city),
        socials = COALESCE(EXCLUDED.socials, profiles.socials),
        updated_at = CURRENT_TIMESTAMP;
    `;
    try {
      await client.queryObject({
        text: query,
        args: [
          profile.address,
          profile.description,
          avatarBytea,
          profile.geoloc?.latitude,
          profile.geoloc?.longitude,
          profile.title,
          profile.city,
          socialJson,
        ],
      });
      ctx.response.status = 200;
      console.log(`Profile ${profile.address} has been updated`);
      ctx.response.body = {
        success: true,
        message: `Profile ${profile.address} has been updated`,
      };
    } catch (error) {
      console.error("Database error in updating profile:", error);
      ctx.response.status = 500;
      ctx.response.body = {
        success: false,
        message: "Internal server error :" + error,
      };
    }
  } catch (error) {
    console.error("Error updating profile:", error);
    ctx.response.status = 500;
    ctx.response.body = {
      success: false,
      message: "Error updating user: " + error,
    };
  }
}
