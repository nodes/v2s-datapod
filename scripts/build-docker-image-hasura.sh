#!/bin/bash

set -e

tag=$1
[[ ! $tag ]] && tag="latest"

docker build -f Dockerfile.Hasura -t duniter-datapod-hasura .
docker image tag duniter-datapod-hasura:$tag poka/duniter-datapod-hasura
docker image push poka/duniter-datapod-hasura:$tag
