#!/bin/bash

export $(cat .env | grep -E 'HASURA_GRAPHQL_ADMIN_SECRET|HASURA_LISTEN_PORT')
endpoint="http://localhost:$HASURA_LISTEN_PORT"

name=$1
[[ ! $name ]] && echo "Please set a name for this migration" && exit 1

hasura migrate create $name --from-server --endpoint $endpoint --admin-secret $HASURA_GRAPHQL_ADMIN_SECRET --database-name default

# To manually apply saved migrations:
# hasura migrate apply --endpoint $endpoint --admin-secret $HASURA_GRAPHQL_ADMIN_SECRET --database-name default